/* bnode.h
   A node class for a binary tree
 */

#ifndef BNODE_H
#define BNODE_H


class BNode {
  public:
    char** board;  // holds the current state of the board
    int parent_state[3]; //[0] = Direction, [1] = segment, [2] = index
    BNode *parent;

    
  public:
    // Purpose: default constructor
    BNode() : parent(NULL),
              board(), 
       	      parent_state()
              {
                board = new char*[Col];
                for (int i = 0; i < Col; i++)
                  board[i] = new char[Row];
              }
    
    // Purpose: parameterized constructor
    // Parameters: specified values for data, left and right child
    // ptrs, and count (respectively)
    // Postconditions: All pointers and data values
    // are set to the specified (or default) values
    BNode(char** data,
          int a_parent_state[3], 
     	    BNode *a_parent)
   
       	  //parent(a_parent),
          {
            parent = a_parent;
            memcpy(parent_state, a_parent_state, sizeof(&a_parent_state));
            
            board = new char*[Col];
            for (int i = 0; i < Col; i++){
			  board[i] = new char[Row];
			  for (int j = 0; j < Row; j++){
				board[i][j] = data[i][j];
			  }
			}

            //memcpy(board, data, sizeof(&data));  
          }

 };

#endif
