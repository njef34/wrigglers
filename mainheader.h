//Main header
//Nick Eggleston


#include <cstdlib>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <cctype>
#include <cstring>
#include <stack>

#define OUT_FILE "solution.txt"
#define MAIN_WRIGGLER_ID 0
#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3
/////////////
#define HEAD 5
#define TAIL 6
/////////////
#define EMPTY 'e'

#define HEAD_SEG 0
#define BODY_SEG 2
#define TAIL_SEG 1

#define PS_DIRECTION 0
#define PS_SEGMENT 1
#define PS_INDEX 2


int Row, Col, NumWrig; //It is important to remember Row == HEIGHT, Col == WIDTH
#include "bnode.h"	   //These must be placed here because they need to know of Row and Col
#include "dllnode.h"
#include "explored_node.h"

int NumMoves = 0; 
BNode *Winner = NULL;
std::stack<BNode*> WinPath; 


