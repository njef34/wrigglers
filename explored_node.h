// Doubly-linked-list for an explored state
// explored.h
// Nick Eggleston

#ifndef EXPLORED_NODE_H
#define EXPLORED_NODE_H



class Explored_Node {
  public:
    Explored_Node* rear;
    char** board;
    
    
    Explored_Node() : rear(NULL)
                      {
                        board = new char*[Col];
                        for (int i = 0; i < Col; i++)
                          board[i] = new char[Row];
                      }
};
#endif
