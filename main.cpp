//////////////////////////////
// This code is the intellectual property of:
//    NICHOLAS J EGGLESTON
// Prepared for the CS357 - Artificial Intelligence 
// Class at Missouri S&T under the direction of
// Dr. Daniel Tauritz
//
// Filename: main.cpp
// Purpose: Contains all the main functions to create a dfst
//      to solve the wriggler problem with one worm
// Start date: 4 Feb 2014
///////////////////////////////

#include "mainheader.h"
using namespace std;


///////////////////////////////////////////////////////////////////////////////
  
Dllnode* findWriggler(char** currentBoard, int index) {
  Dllnode *head, *tail, *body;
  bool headFound = false;
  char above, below, right, left;

  for (int i = 0; i < Row; i++){
    for (int j = 0; j < Col; j++){
      if (currentBoard[i][j] == index + '0') {  // Go through board and find r,c of the tail.
        tail = new Dllnode;
        tail->row = i; 
        tail->col = j;
        tail->headTailBody = TAIL_SEG;
        tail->rear = NULL;
        tail->front = NULL;
        i = Row;  // This quits the for(i) loop.
        j = Col;  // This quits the for(j) loop.
      }
	}
  }

  // We now have the tail.  Let's work our way back to the head.
  for (body = tail; !headFound; body = head) {
    above = (body->row > 0)       ? currentBoard[body->row - 1][body->col] : 'x'; 
    below = (body->row < Row - 1) ? currentBoard[body->row + 1][body->col] : 'x';
    left  = (body->col > 0)       ? currentBoard[body->row][body->col - 1] : 'x';
    right = (body->col < Col - 1) ? currentBoard[body->row][body->col + 1] : 'x';
    head = new Dllnode;  // Build the next segment of the wriggler.
    head->front = NULL;
    head->rear  = body;
    body->front = head;
    head->row = body->row;  // Initialize head same as body, but later alter as needed.
    head->col = body->col;  // Initialize head same as body, but later alter as needed.
    head->headTailBody = BODY_SEG;
    if (above == 'v' || above == 'D')
      head->row--;
    else if (below == '^' || below == 'U')
      head->row++;
    else if (right == '<' || right == 'L')
      head->col++; //head->col--;
    else  // (left == '>' || left == 'R')
      head->col--; //head->col++;
    if (above == 'D' || below == 'U' || right == 'L' || left == 'R') {  // head is the actual head.  We're done.
      head->headTailBody = HEAD_SEG;
      headFound = true;
    }
  }
  return head;
}

///////////////////////////////////////////////////////////////////////////////

void cameFrom (BNode* current){
  if (current != NULL) {
    WinPath.push(current);
    cameFrom(current->parent);
  }
}

///////////////////////////////////////////////////////////////////////////////
    
void writeSolutionFile(clock_t t){ 
  Dllnode *head = NULL;
  ofstream fout;
  fout.open(OUT_FILE);
  
  while(!WinPath.empty()){
    NumMoves++;
    fout << WinPath.top()->parent_state[PS_INDEX] << ' '; //Output index of moved wriggler
    
    head = findWriggler(WinPath.top()->board, (int)WinPath.top()->parent_state[PS_INDEX]);
    if (WinPath.top()->parent_state[PS_SEGMENT] == HEAD){
        fout << HEAD_SEG << ' ' << head->col << ' ' << head->row <<endl; //Output the head moved and where
      
      }
    else{
      while(head->rear != NULL)
        head = head->rear;  //This moves the head pointer to the tail and saves us from creating another pointer
      fout << TAIL_SEG << ' ' << head->col << ' ' << head->row <<endl; //Output the tail moved and where
    }
       
    WinPath.pop();
  }
  for (int i = 0; i < Row; i++){
    for (int j = 0; j < Col; j++){
      fout << Winner->board[i][j] << ' ';    
    }
    fout << '\n';
  }
  fout << t << '\n';
  fout << NumMoves;
  fout.close();
}


///////////////////////////////////////////////////////////////////////////////

bool legalMove(char** currentBoard, int direction, int segment, int index){
  Dllnode *head = findWriggler(currentBoard, index);
  Dllnode *temp = head->rear;
  int hr, hc, tr, tc;
  
  while(temp->rear != NULL)
    temp = temp->rear;
      
  hr = head->row; hc = head->col;
  tr = temp->row; tc = temp->col;
    

  switch(direction){
    case(UP):
      if(segment == HEAD)
        return (hr > 0 && currentBoard[hr-1][hc] == EMPTY);
      else 
        return(tr > 0 && currentBoard[tr-1][tc] == EMPTY);
      break;
    //////////////////
    case(DOWN):
      if(segment == HEAD)
        return(hr+1 < Row && currentBoard[hr+1][hc] == EMPTY);
      else 
        return(tr+1 < Row && currentBoard[tr+1][tc] == EMPTY);
      break;
    /////////////////
    case(LEFT):
      if(segment == HEAD)
        return(hc > 0 && currentBoard[hr][hc-1] == EMPTY);
      else 
        return(tc > 0 && currentBoard[tr][tc-1] == EMPTY);
      break;
    /////////////////
    case(RIGHT):
      if(segment == HEAD)
        return(hc+1 < Col && currentBoard[hr][hc+1] == EMPTY);
      else 
        return(tc+1 < Col && currentBoard[tr][tc+1] == EMPTY);
      break;
  }


}

///////////////////////////////////////////////////////////////////////////////

bool isWin(char** currentBoard){
  Dllnode *head = findWriggler(currentBoard, MAIN_WRIGGLER_ID);
  return ((head->row == Row-1 && head->col == Col-1)  ||
    currentBoard[Row-1][Col-1] == MAIN_WRIGGLER_ID);
}

///////////////////////////////////////////////////////////////////////////////

char** newBoard(char** currentBoard, int direction, int segment, int index){
  Dllnode *head, *tail, *temp;
  char** tempBoard;
  int oldr, oldc;
  
  //Build tempBoard
  tempBoard = new char*[Col];
  for (int i = 0; i < Col; i++)
    tempBoard[i] = new char[Row];
  
  //Initialize our tempBoard to a copy of currentBoard
  for(int i = 0; i < Row; i++)
    for(int j = 0; j < Col; j++)  
      tempBoard[i][j] = currentBoard[i][j];
      
  head = findWriggler(currentBoard, index);
  temp = head;
  while(temp->rear != NULL)
    temp = temp->rear;
  
  tail = temp;
  
  if(segment == HEAD)
    {oldr = tail->row; oldc = tail->col;}
  else 
    {oldr = head->row; oldc = head->col;}
 
  
  //Now that we know where the wriggler is currently,
  //We'll move the head or the tail around
  if(segment == HEAD){ //This moves the head
    temp = new Dllnode;
    temp->headTailBody = HEAD_SEG;
    switch (direction){
      case(UP):
        temp->row = head->row-1;
        temp->col = head->col;
        break;
      
      case(DOWN):
        temp->row = head->row+1;
        temp->col = head->col;
        break;
      
      case(LEFT):
        temp->row = head->row;
        temp->col = head->col-1;
        break;
      
      case(RIGHT):
        temp->row = head->row;
        temp->col = head->col+1;
        break;
    }
    head->headTailBody = BODY_SEG;
    head->front = temp;
    temp->rear = head;
    head = temp;

    temp = tail->front;
    temp->headTailBody = TAIL_SEG;
    temp->rear = NULL;
    delete tail;
    tail = temp;
  } else { //This moves the tail
    temp = new Dllnode;
    temp->headTailBody = TAIL_SEG;
    switch (direction){
      case(UP):
        temp->row = tail->row-1;
        temp->col = tail->col;
        break;
      
      case(DOWN):
        temp->row = tail->row+1;
        temp->col = tail->col;
        break;
      
      case(LEFT):
        temp->row = tail->row;
        temp->col = tail->col-1;
        break;
      
      case(RIGHT):
        temp->row = tail->row;
        temp->col = tail->col+1;
        break; 
    }
    tail->headTailBody = BODY_SEG;
    tail->rear = temp;
    temp->front = tail;
    tail = temp;

    temp = head->rear;
    temp->headTailBody = HEAD_SEG;
    temp->front = NULL;
    delete head;
    head = temp;
  }
    
  //Here we put everything back into "board form"
  //as opposed to just a list of coords for the wriggler
  for(temp = head; temp != NULL; temp = temp->rear) {
    switch (temp->headTailBody){
      case (HEAD_SEG):
        if (temp->row > temp->rear->row)
          tempBoard[temp->row][temp->col] = 'U';
        else if (temp->row < temp->rear->row)
          tempBoard[temp->row][temp->col] = 'D';
        else if ((temp->col > temp->rear->col))
          tempBoard[temp->row][temp->col] = 'L';
        else 
          tempBoard[temp->row][temp->col] = 'R';

        break;
      
      case(TAIL_SEG):
        tempBoard[temp->row][temp->col] = index + '0';    
        break;
      
      case (BODY_SEG):
        if (temp->row > temp->rear->row)
          tempBoard[temp->row][temp->col] = '^';
        else if (temp->row < temp->rear->row)
          tempBoard[temp->row][temp->col] = 'v';
        else if ((temp->col > temp->rear->col))
          tempBoard[temp->row][temp->col] = '<';
        else 
          tempBoard[temp->row][temp->col] = '>';

        break;
    }
  }

  tempBoard[oldr][oldc] = EMPTY;

  
  return tempBoard;
}
///////////////////////////////////////////////////////////////////////////////

bool boardsAreSame(char **b1, char **b2) {
  int i, j;

  for (i = 0; i < Row; i++)
    for (j = 0; j < Col; j++)
      if (b1[i][j] != b2[i][j])
        return false;
  return true;
}

///////////////////////////////////////////////////////////////////////////////

void buildTree(BNode* root, char** currentBoard, int maxDepth, int level, Explored_Node* eHead) {
  Explored_Node* eTemp = NULL;
  if (isWin(currentBoard)){
    Winner = root; 
    return;
  }
  
  if (level <= maxDepth){
    BNode* temp = NULL;
    int parent_state[3];
    bool isNew;
    
    for (int i = 0; i < NumWrig; i++) {
      for (int j = HEAD; j <= TAIL; j++) {
        for (int k = UP; k <= RIGHT; k++) {
          if(legalMove(currentBoard, k, j, i)) {
            temp = new BNode(newBoard(currentBoard, k, j, i), parent_state, root);
            temp->parent_state[PS_DIRECTION] = k;
            temp->parent_state[PS_SEGMENT] = j;
            temp->parent_state[PS_INDEX] = i;
            
            for (eTemp = eHead, isNew = true; eTemp->rear != NULL && isNew; eTemp = eTemp->rear) {
              if (boardsAreSame(temp->board, eTemp->board))
                isNew = false;
            }
            if (isNew) {
              eTemp->rear = new Explored_Node();
              for (int i = 0; i < Row; i++)
                for (int j = 0; j < Col; j++)
                  eTemp->rear->board[i][j] = temp->board[i][j];
              buildTree(temp, temp->board, maxDepth, level+1, eHead);
              if (Winner != NULL) //If we found a winner, quit recursing
                return;
              else { //We didn't find a winner but we need to get rid of the states we created in this branch
                delete eTemp->rear;
                eTemp->rear = NULL;
              }
            }
          } 
        }
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[]) {
  clock_t t = clock();
  char temp[1000];
  char** currentBoard;
  Explored_Node* root = NULL;

  ifstream fin;
  fin.open(argv[1]);

  fin.read(temp, 1000);
  fin.close();
  Col = temp[0]-'0'; //Col is the 0th char in the file, -'0' converts to decimal from ASCII  
  Row = temp[2]-'0'; //Row is the 2nd char in the file
  NumWrig = temp[4]-'0'; //You get the gist
  
  //create board
  currentBoard = new char*[Col];
  for (int i = 0; i < Col; i++)
    currentBoard[i] = new char[Row];
  
  cout<< "Welcome to Nick's wriggler solver, here is your current board: "<<endl;
  int a = 6;
  for(int i = 0; i < Row; i++, a++){
    for(int j = 0; j < Col; j++, a++) {
        while(temp[a] == ' ')
          a++;
        currentBoard[i][j] = temp[a];
        cout << currentBoard[i][j];
    }
  cout <<endl; 
  }
  cout<< "Standby for solution in solution.txt..."<<endl;
  root = new Explored_Node;
  root->board = currentBoard;
  for(int i = 0; Winner == NULL; i++)
    buildTree(NULL, currentBoard, i, 0, root);

  cameFrom(Winner);
  writeSolutionFile(clock() - t); 

  return 0;  
}
 
