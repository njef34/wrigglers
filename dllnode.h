// Doubly-linked-list node
// dllnode.h
// Nick Eggleston

#ifndef DLLNODE_H
#define DLLNODE_H



class Dllnode {
  public:
    Dllnode* front;
    Dllnode* rear;
    int row;
    int col;
    int headTailBody; //Head = 0, Tail = 1, Body = 2
    
    Dllnode() : front(NULL),
                rear(NULL),
                row(0),
                col(0),
                headTailBody(2) //Defaults to body, will change later if nec.
                {}

	Dllnode& operator=(const Dllnode& rhs){
	  front = rhs.front;
	  rear = rhs.rear;
	  row = rhs.row;
	  col = rhs.col;
	  headTailBody = rhs.headTailBody;
	  return *this;
	}
};
#endif 
